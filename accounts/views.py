from django.shortcuts import render
from .forms import RegisterForm, EditProfileForm
from django.contrib.auth import get_user_model, authenticate, login

def profile(request):

    username = request.user.username
    first_name = request.user.first_name
    last_name = request.user.last_name
    email = request.user.email
    date_joined = request.user.date_joined

    return render(request, 'accounts/profile.html',
                {'username': username,
                 'first_name' : first_name,
                 'last_name' : last_name,
                 'email' : email,
                 'date_joined' : date_joined
                 })


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            raw_password = form.cleaned_data.get('password1')

            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('profile')
    form = RegisterForm()
    return render(request, 'accounts/register.html', {'form': form} )

def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            user = form.save()
            return redirect('profile')
    form = EditProfileForm(instance=request.user)
    return render(request, 'accounts/edit_profile.html', {'form': form} )
