from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('profile/', views.profile, name='profile'),
    path('login/',
        auth_views.LoginView.as_view(template_name="accounts/login.html"),
        name='login'),
    path('logout/',
    auth_views.LogoutView.as_view
    (
        template_name="accounts/logged_out.html",
        next_page="/worldly/"

    ),
    name='logout'),
    path('register/', views.register, name='register'),
    path('profile/edit/', views.edit_profile, name='edit_profile'),
    ]
