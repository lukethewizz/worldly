from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('worldly/', views.event_listing, name='events'),
    path('worldly/<str:name>', views.event_detail, name='detail'),
]
