from django.shortcuts import render
from django.http import HttpResponse

from .models import Event


def index(request):
    return render(request,'worldly/index.html')

def event_detail(request, name):
    event = Event.objects.get(name=name)

    return render(request, 'worldly/event_detail.html', {'event': event})


def event_listing(request):

    events = Event.objects.all()

    return render(request, 'worldly/event_listing.html', {'events': events})
